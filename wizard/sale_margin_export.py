# -*- encoding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from openerp import api, models, fields, _
from datetime import datetime, date

class SaleOrderMarginReport(models.TransientModel):
    _name = 'sale.order.margin.report'

    date_from = fields.Date('From')
    date_to = fields.Date('To')

    @api.multi
    def print_report(self):
        self.ensure_one()
        [data] = self.read()
        return {
                'name':'Sale Order Margin',
                'type': 'ir.actions.report.xml',
                'report_name': 'sale_order_margin_report.margin_xlsx_report',
                'datas': data,
            }
