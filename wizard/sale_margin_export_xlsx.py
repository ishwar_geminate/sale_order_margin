# -*- encoding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from openerp.addons.report_xlsx.report.report_xlsx import ReportXlsx
from openerp.report import report_sxw
from datetime import datetime, date, timedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF
import xlsxwriter


class SaleMarginOrderReport(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, objects):

        DateFrom = datetime.strptime(str(data.get('date_from', False)), '%Y-%m-%d')
        DateTo = datetime.strptime(str(data.get('date_to', False)), '%Y-%m-%d')
        DateTo = str(DateTo).split(" ")[0] + ' 24:00:00'
        SaleOrder = self.env['sale.order'].search([('state','=','done'), 
                                ('date_order','>=',str(DateFrom)),('date_order','<=', str(DateTo))])

        SalePerson = []
        for Person in SaleOrder:
            if Person.user_id:
                SalePerson.append(Person.user_id.id)
        SalePerson = list(set(SalePerson))
        worksheet = workbook.add_worksheet('Sale Order Margin')
        title_header = workbook.add_format({'bold': True,'valign':'vcenter','font_size':18, 'align':'center'})
        header_format = workbook.add_format({'bold': True,'valign':'vcenter','font_size':16})
        cell_wrap_format = workbook.add_format({'valign':'vjustify','font_size':12,})
        title_format = workbook.add_format({'valign': 'vcenter', 'font_size':14})
        special_format = workbook.add_format({'bold': True, 'valign': 'vcenter', 'font_size':14})
        title_format_bg = workbook.add_format({'bg_color':'gray'})
        worksheet.set_row(1,30); worksheet.set_row(2,35)
        worksheet.merge_range(0, 0, 1, 9, str(self.env.user.company_id.name), title_header)
        worksheet.merge_range(2, 0, 3, 9, 'Sale Order Margin Analysis ' + str(data.get('date_from', False)) + ' to '
                              + str(data.get('date_to', False)), title_header)

        worksheet.set_row(4,25)
        worksheet.set_column('E:E', 20); worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 25); worksheet.set_column('I:I', 10)
        worksheet.merge_range(4, 0, 4, 3, 'Sales Person', header_format)
        worksheet.write(4, 4, 'Margin', header_format)
        worksheet.write(4, 5, 'Margin % ', header_format)
        worksheet.merge_range(4, 6, 4, 7, 'Sale Amount(Exclude Vat)', header_format)
        worksheet.merge_range(4, 8, 4, 9, 'Order Date', header_format)
        row = 5; worksheet.set_row(row,25)

        for User in self.env['res.users'].browse(SalePerson):
            worksheet.merge_range(row, 0, row, 3, str(User.name), title_format)
            worksheet.merge_range(row, 4, row, 9, ' ', title_format)
            TotalMargin = 0.0; TotalLine = 0; TotalPer = 0.0; TotalAmount = 0.0
            for order in SaleOrder:
                if order.user_id.id == User.id:
                    worksheet.merge_range(row + 1 , 0, row + 1, 3, " ", cell_wrap_format)
                    worksheet.write(row + 1, 4, str(order.margin_calc) , cell_wrap_format)
                    worksheet.write(row + 1, 5, str("%.2f" % order.margin_cust) + ' % ', cell_wrap_format)
                    worksheet.merge_range(row + 1 , 6, row + 1, 7,  str(order.amount_untaxed) , cell_wrap_format)
                    worksheet.merge_range(row + 1, 8, row + 1, 9, str(order.date_order).split(" ")[0], cell_wrap_format)
                    row += 1; TotalLine += 1; TotalAmount += order.amount_untaxed; TotalPer += order.margin_cust; TotalMargin += order.margin_calc
            TotalPer = TotalPer / TotalLine; worksheet.set_row(row +1 ,25)
            worksheet.merge_range(row + 1, 0, row + 1, 3, 'Total', special_format)
            worksheet.write(row + 1, 4, str(TotalMargin) + ' ' + str(self.env.user.company_id.currency_id.name) , special_format)
            worksheet.write(row + 1, 5, str( "%.2f" % TotalPer) + ' AVG. % ', special_format)
            worksheet.merge_range(row + 1, 6,row + 1, 7, str(TotalAmount) + ' ' + str(self.env.user.company_id.currency_id.name) , special_format)
            worksheet.merge_range(row + 1, 8, row + 1, 9, ' ', title_format)
            worksheet.merge_range(row + 2, 0, row + 2, 9, ' ', title_format_bg)
            row += 3

SaleMarginOrderReport('report.sale_order_margin_report.margin_xlsx_report', 'sale.order.margin.report',parser=report_sxw.rml_parse)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
